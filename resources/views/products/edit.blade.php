@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Edit product</div>

                <div class="panel-body">
                    @foreach ($errors->all() as $error)
                        <p class="error">{{ $error }}</p>
                    @endforeach
                    <form name="sentMessage" method="POST" action="{{ action('ProductsController@update',['product'=> $product->id]) }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="control-group">
                            <div class="form-group floating-label-form-group controls">
                                <label>Name</label>
                                <input type="text" class="form-control" placeholder="Name" id="name" name="name" value="{{$product->name}}">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="form-group floating-label-form-group controls">
                                <label>Description</label>
                                <textarea rows="5" class="form-control" placeholder="Message" id="description" name="description">{{$product->description}}</textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <br>
                        <div class="input_fields_wrap">
                            @foreach ($product->prices as $price)
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="price" id="price" name="price[{{$price->id}}][value]" value="{{$price->value}}" >
                                </div>
                            @endforeach
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" id="sendMessageButton">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
