@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Create product</div>

                <div class="panel-body">
                    @foreach ($errors->all() as $error)
                        <p class="error">{{ $error }}</p>
                    @endforeach
                    <form name="sentMessage" method="POST" action="{{ action('ProductsController@store') }}">
                        {{ csrf_field() }}
                        <div class="control-group">
                            <div class="form-group floating-label-form-group controls">
                                <label>Name</label>
                                <input type="text" class="form-control" placeholder="Name" id="name" name="name" >
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="form-group floating-label-form-group controls">
                                <label>Description</label>
                                <textarea rows="5" class="form-control" placeholder="Message" id="description" name="description"></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>

                        <div class="input_fields_wrap">
                            <div class="form-group">
                                <button class="add_field_button btn btn-success btn-xs">Add More Fields</button>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="price" id="price" name="price[][value]" >
                            </div>
                        </div>

                        <div id="success"></div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" id="sendMessageButton">Add</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('js/mine.js') }}"></script>
@endsection