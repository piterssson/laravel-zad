<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProducts;
use App\Products as ProductsModel;
use App\Price as PriceModel;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ProductsModel $productsModel)
    {
        return view('products/index', ['products' => $productsModel->all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProducts $request, ProductsModel $products, PriceModel $price)
    {
        $products->fill($request->all());
        $products->save();
        $prices = [];
        foreach ($request->get('price') as $value) {
            $prices[] = new $price($value);
        }
        $products->prices()->saveMany($prices);

        return redirect('products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, ProductsModel $productsModel)
    {
        return view('products/edit', ['product' => $productsModel->with('prices')->find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateProducts $request, $id, ProductsModel $productsModel, PriceModel $price)
    {
        $products = $productsModel->find($id);
        $products->fill($request->all());
        $products->save();
        foreach ($request->get('price') as $key => $value) {
            $price->find($key)->update($value);
        }

        return redirect('products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, ProductsModel $productsModel)
    {
        $productsModel->find($id)->delete();

        return redirect('products');
    }
}
